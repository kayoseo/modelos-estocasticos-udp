function exp_geo(prob,lambda,num)
comp =1-prob; 
expo=zeros(num,1); %arreglo resultados de la Exponencial
geo=zeros(num,1); %arreglo resultados de la Geometrica
X=[1:num]; 

for i =X
   expo(i)=lambda*exp(-lambda*X(i));
   geo(i)=(comp)^(X(i)-1)*prob;
end
 
hold on
grid on
plot (X,expo,'r')
plot (X,geo,'g')
end