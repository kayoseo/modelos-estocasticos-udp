function modelos()
clear
clc
close all

n = input ('Please Enter Number of Exams : '); %numero de pruebas, por ejemplo, tirar un dado n veces
p = input ('Please Enter Probability : '); % probabilidad de éxito

eta=n*p; %esperanza
sigma=sqrt(n*p*(1-p)); % desviación estandar(raiz cuadrada de la varianza
L=n*p;
for x=1:n*100; 
a=rand(1,n);
b=0;
for i=1:n
    if(a(i)>=1-p) % recorre el vector evaluando si cada intento es mayor a q, es es 1-p
        b=b+1; % acumula uno cada vez que cumple ser mayor a la probabilidad complementaria(q)
    end
end
c(x)=b;
d(x)=binornd(n , p) % distribucion binomial
e(x)=normpdf(x,eta,sqrt(eta*(1-p)));
p(x)= ( (L^x) * exp(-L) ) / ( factorial(x) );
end

    
   
E=sigma*e+eta;

h=histfit(c);
set(h(1),'facecolor','y'); set(h(2),'color','r')
axis([0 n 0 inf])
M = mean(c); %valor promedio del vector c, que es b
varc=var(c); % la varianza del mismo vector
disp(['var is : ', num2str(varc)]);
disp(['Mean is : ', num2str(M)]);
figure

h2=histfit(d);%Binomail
set(h2(1),'facecolor','y'); set(h2(2),'color','r')
axis([0 n 0 inf])
M = mean(d); %valor promedio del vector c, que es b
varc=var(d); % la varianza del mismo vector
disp(['var is : ', num2str(var2)]);
disp(['Mean is : ', num2str(M2)]);
figure

h4=histfit(e); %Normal
set(h3(1),'facecolor','g'); set(h3(2),'color','r')
axis([0 n 0 inf])
M3 = mean(e);
varc3=var(e);
disp(['var is : ', num2str(varc3)]);
disp(['Mean is : ', num2str(M3)]);
figure

h3=histfit(p); %Poisson
set(h3(1),'facecolor','g'); set(h3(2),'color','r')
axis([0 n 0 inf])
M3 = mean(p);
varc3=var(p);
disp(['var is : ', num2str(varc3)]);
disp(['Mean is : ', num2str(M3)]);
figure




h3=histfit(p); %al igual que arriba pero con binomial
set(h3(1),'facecolor','g'); set(h3(2),'color','r')
axis([0 n 0 inf])
M3 = mean(p);
varc3=var(p);
disp(['var is : ', num2str(varc3)]);
disp(['Mean is : ', num2str(M3)]);
figure
